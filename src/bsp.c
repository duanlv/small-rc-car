#include "bsp.h"
#include "stm8s_tim4.h"

uint8_t delay_time = 0;

void bsp_init(void)
{
    CLK_DeInit();
    CLK_SYSCLKConfig(CLK_PRESCALER_CPUDIV1);
    CLK_SYSCLKConfig(CLK_PRESCALER_HSIDIV1);
    /* Configure the system clock to use HSE clock source and to run at 8Mhz */
    (void)CLK_ClockSwitchConfig(CLK_SWITCHMODE_AUTO, CLK_SOURCE_HSE, DISABLE, 
        CLK_CURRENTCLOCKSTATE_DISABLE);

    TIM4_TimeBaseInit(TIM4_PRESCALER_8, 255);
    TIM4_ClearFlag(TIM4_FLAG_UPDATE);
    TIM4_ITConfig(TIM4_IT_UPDATE, ENABLE);
    TIM4_Cmd(ENABLE);
    enableInterrupts();
}

void delay_us(uint8_t microseconds)
{
    delay_time = microseconds;
    while(delay_time != 0);
}