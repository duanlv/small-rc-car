/**
  ******************************************************************************
  * @file    Project/main.c 
  * @author  MCD Application Team
  * @version V2.3.0
  * @date    16-June-2017
  * @brief   Main program body
   ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2014 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */ 


/* Includes ------------------------------------------------------------------*/
#include "stm8s.h"
#include "motor.h"
#include "ultrasonic.h"
#include "mpu6050.h"
#include "bsp.h"

/* Private defines -----------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/


typedef void *func_ptr_t(int event);

typedef enum _E_MOTOR_EVENT_ {
  MOTOR_E_MV_FW = 0,
  MOTOR_E_MV_BW,
  MOTOR_E_T_LEFT,
  MOTOR_E_T_RIGH,
  MOTOR_E_STOP
} E_MOTOR_EVENT;

typedef enum _E_MOTOR_STS_ {
    MOTOR_STS_STOP = 0,
    MOTOR_STS_MV_FW,
    MOTOR_STS_MV_BW,
    MOTOR_STS_T_LEFT,
    MOTOR_STS_T_RIGH
} E_MOTOR_STS;

static int motor_sts = MOTOR_STS_STOP;
static void motor_hdl_init(void)
{
  motor_init();
  motor_set_speed(3U);
  motor_sts = MOTOR_STS_STOP;
}

static void motor_control(int event) 
{
    // motor_mtx[motor_sts][event]();
    switch (motor_sts) {
      case MOTOR_STS_STOP:
        if (event == MOTOR_E_MV_FW) {
          motor_forward();
          motor_sts = MOTOR_STS_MV_FW;
        } else {
          //T.B.D
        }
        break;
      case MOTOR_STS_MV_FW:
        if (event == MOTOR_E_STOP) {
          motor_brake();
          motor_sts = MOTOR_STS_STOP;
        } else {
          //T.B.D
        }
        break;
      default:
        break;
    }
}

void main(void)
{
  float distance;
  uint8_t dev_id = 0;

  bsp_init();
  ultrasonic_init();
  motor_hdl_init();
  MPU6050_Initialize();

  dev_id = MPU6050_GetDeviceID();

  /* Infinite loop */
  while (1) {
    distance = ultrasonic_read();
    if (distance >= 1.0) {
      motor_control(MOTOR_E_MV_FW);
    } else {
      motor_control(MOTOR_E_STOP);
    }
  }
}

#ifdef USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param file: pointer to the source file name
  * @param line: assert_param error line source number
  * @retval : None
  */
void assert_failed(u8* file, u32 line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
