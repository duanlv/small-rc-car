#include "stm8s_gpio.h"
// #include "util.h"
#include "bsp.h"

#define TIM2_FREQ   1000000UL

void ultrasonic_init(void)
{
   GPIO_Init(GPIOD, GPIO_PIN_2, GPIO_MODE_OUT_PP_LOW_SLOW);
   TIM2_DeInit();
   TIM2_TimeBaseInit(TIM2_PRESCALER_1, 999);
   
   TIM1_ICInit(TIM1_CHANNEL_1,
            TIM1_ICPOLARITY_RISING,
            TIM1_ICSELECTION_DIRECTTI,
            TIM1_ICPSC_DIV8,
            0);
    TIM1_ICInit(TIM1_CHANNEL_2,
            TIM1_ICPOLARITY_FALLING,
            TIM1_ICSELECTION_INDIRECTTI,
            TIM1_ICPSC_DIV4,
            0);    

    TIM2_ClearFlag(TIM2_FLAG_CC1);
    TIM2_ClearFlag(TIM2_FLAG_CC2);
    TIM2_Cmd(ENABLE);
//   enableInterrupts();
}

float ultrasonic_read(void)
{
    float distance;
    uint16_t rising_timestamp, falling_timestamp;
    
    TIM2_ClearFlag(TIM2_FLAG_CC1);
    TIM2_ClearFlag(TIM2_FLAG_CC2);
    
    GPIO_WriteHigh(GPIOD, GPIO_PIN_2);
    delay_us(10);
    GPIO_WriteLow(GPIOD, GPIO_PIN_2);
    
    while(TIM2_GetFlagStatus(TIM2_FLAG_CC1));
    rising_timestamp = TIM2_GetCapture1();
    while(TIM2_GetFlagStatus(TIM2_FLAG_CC2));
    falling_timestamp = TIM2_GetCapture1();
    
    distance = ((falling_timestamp - rising_timestamp) * 0.034)/2;
    
    return distance;
}                                                                                                                                                        