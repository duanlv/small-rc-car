#include "motor.h"
#include "stm8s_gpio.h"
#include "stm8s_tim1.h"
#include "stm8s_tim2.h"

#define MOTOR_PORT  GPIOC
#define MOTOR_PINS  (GPIO_Pin_TypeDef)(GPIO_PIN_2 | \
                     GPIO_PIN_3 | \
                     GPIO_PIN_4 | \
                     GPIO_PIN_5 | \
                     GPIO_PIN_6)

#define MOTOR_PIN_AIN1  GPIO_PIN_2
#define MOTOR_PIN_AIN2  GPIO_PIN_3
#define MOTOR_PIN_STBY  GPIO_PIN_4
#define MOTOR_PIN_BN1   GPIO_PIN_5
#define MOTOR_PIN_BN2   GPIO_PIN_6

#define PERIOD          ((uint16_t)4096)
#define DUTY_CYCLE      ((uint16_t)2047)

static uint16_t ccr_vals[] = {
    511,        // 25%
    1023,
    1535,       
    2047,
    4096        //100%
};

static void pwm_init(void)
{
    TIM1_DeInit();
    /* Time Base configuration */
    TIM1_TimeBaseInit(0, TIM1_COUNTERMODE_UP, PERIOD, 0);

    /*TIM1_Pulse = CCR1_Val*/
    TIM1_OC1Init(TIM1_OCMODE_PWM2, TIM1_OUTPUTSTATE_ENABLE, TIM1_OUTPUTNSTATE_ENABLE,
        DUTY_CYCLE, TIM1_OCPOLARITY_LOW, TIM1_OCNPOLARITY_HIGH, TIM1_OCIDLESTATE_SET,
        TIM1_OCNIDLESTATE_RESET); 
    
    /*TIM1_Pulse = CCR2_Val*/
    TIM1_OC2Init(TIM1_OCMODE_PWM2, TIM1_OUTPUTSTATE_ENABLE, TIM1_OUTPUTNSTATE_ENABLE, 
        DUTY_CYCLE, TIM1_OCPOLARITY_LOW, TIM1_OCNPOLARITY_HIGH, TIM1_OCIDLESTATE_SET, 
        TIM1_OCNIDLESTATE_RESET);
               
    /* TIM1 counter enable */
    TIM1_Cmd(ENABLE);
    
    /* TIM1 Main Output Enable */
    TIM1_CtrlPWMOutputs(ENABLE);
}


void motor_init(void)
{
    GPIO_Init(MOTOR_PORT, MOTOR_PINS,  GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_WriteLow(MOTOR_PORT, MOTOR_PINS);
    
    pwm_init();
}

void motor_set_motor(uint8_t motor, uint8_t dir, uint16_t speed)
{
    //T.B.D
}

void motor_set_speed(uint8_t speed)
{
    uint16_t CCR_Val = ccr_vals[speed];
    
     /* TIM1 Main Output disable*/
    TIM1_CtrlPWMOutputs(DISABLE);
               
    /* TIM1 counter disable*/
    TIM1_Cmd(DISABLE);
    
    /* TIM1 Main Output Enable */
    TIM1_OC1Init(TIM1_OCMODE_PWM2, TIM1_OUTPUTSTATE_ENABLE, TIM1_OUTPUTNSTATE_ENABLE,
        CCR_Val, TIM1_OCPOLARITY_LOW, TIM1_OCNPOLARITY_HIGH, TIM1_OCIDLESTATE_SET,
        TIM1_OCNIDLESTATE_RESET); 
    
    /*TIM1_Pulse = CCR2_Val*/
    TIM1_OC2Init(TIM1_OCMODE_PWM2, TIM1_OUTPUTSTATE_ENABLE, TIM1_OUTPUTNSTATE_ENABLE, 
        CCR_Val, TIM1_OCPOLARITY_LOW, TIM1_OCNPOLARITY_HIGH, TIM1_OCIDLESTATE_SET, 
        TIM1_OCNIDLESTATE_RESET);
               
    /* TIM1 counter enable */
    TIM1_Cmd(ENABLE);
    
    /* TIM1 Main Output Enable */
    TIM1_CtrlPWMOutputs(ENABLE);   
}

void motor_forward(void)
{
    GPIO_WriteHigh(MOTOR_PORT, MOTOR_PIN_STBY);
    GPIO_WriteHigh(MOTOR_PORT, (GPIO_Pin_TypeDef)(MOTOR_PIN_AIN1 | MOTOR_PIN_BN1));
}

void motor_backward(void)
{
    GPIO_WriteHigh(MOTOR_PORT, MOTOR_PIN_STBY);
    GPIO_WriteHigh(MOTOR_PORT, (GPIO_Pin_TypeDef)(MOTOR_PIN_AIN2 | MOTOR_PIN_BN2));
}

void motor_turn_left(void)
{
    //T.B.D
}

void motor_turn_right(void)
{
    //T.B.D
}

void motor_stop(void)
{
    GPIO_WriteLow(MOTOR_PORT, MOTOR_PINS);
    // TIM1_ControlPwmOutputs(DISABLE);
    // TIM1_Cmd(DISABLE);
}

void motor_brake(void)
{
    GPIO_WriteLow(MOTOR_PORT, (GPIO_Pin_TypeDef)(MOTOR_PIN_AIN2 | MOTOR_PIN_BN2));
    GPIO_WriteLow(MOTOR_PORT, (GPIO_Pin_TypeDef)(MOTOR_PIN_AIN1 | MOTOR_PIN_BN1));
}

