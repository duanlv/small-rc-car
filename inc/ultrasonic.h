#ifndef _ULTRASONIC_H_
#define _ULTRASONIC_H_

void ultrasonic_init(void);
float ultrasonic_read(void);

#endif //_ULTRASONIC_H_
