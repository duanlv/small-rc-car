#ifndef _BSP_H_
#define _BSP_H_

#include "stm8s.h"

void bsp_init(void);
void delay_us(uint8_t microseconds);

#endif // _BSP_H_
