#ifndef _UTIL_H_
#define _UTIL_H_

#if 0
// #include <stdint.h>
#include <intrinsics.h>

#ifndef F_CPU
#warning F_CPU is not defined!
#endif



#define T_COUNT(x) (( F_CPU * x / 1000000UL )-5)/5)

static inline void _delay_cycl( unsigned short __ticks )
{
	asm("nop\n nop\n"); 
    do { 		// ASM: ldw X, #tick; lab$: decw X; tnzw X; jrne lab$
        __ticks--;//      2c;                 1c;     2c    ; 1/2c   
        } while ( __ticks );
    asm("nop\n");    
}

static inline void _delay_us( const unsigned short __us )
{
	_delay_cycl( (unsigned short)( T_COUNT(__us) );	
}
                      
static inline void delay_ms( unsigned short __ms )
{
	while ( __ms-- )
	{
		_delay_us( 1000 );
	}
}

#define delay_us(x)     _delay_us(x)


#endif 
#endif //_UTIL_H_
