#ifndef _MOTOR_H_
#define _MOTOR_H_

#define MOVE_FORWARD    0U
#define MOVE_BACKWAR    1U 

// #include <stdint.h>
#include <stm8s.h>

void motor_init(void);
void motor_set_speed(uint8_t speed);
void motor_set_motor(uint8_t motor, uint8_t dir, uint16_t speed);
// void motor_set_speeds(uint8_t lspeed, uint8_t rspeed);
void motor_forward(void);
void motor_backward(void);
// void motor_move(uint8_t dir);
void motor_turn_left(void);
void motor_turn_left(void);
void motor_turn_right(void);
void motor_stop(void);
void motor_brake(void);

#endif //_MOTOR_H_


